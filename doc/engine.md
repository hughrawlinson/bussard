# Ceres Shipyards Omega Engine Booster

The Ceres Shipyards Omega Engine Booster is a powerful upgrade which
increases your ship's fuel consumption in order to significantly boost
the thrust it is capable of.

Since it is a passive upgrade, no configuration or activation is
necessary to take advantage of its functionality.

Copyright © 2411 Ceres Shipyards, All Rights Reserved.
