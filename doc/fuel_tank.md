# Songket Shipyards Hermes Fuel Tank

The Songket Shipyards Hermes Fuel Tank gives your ship an additional
fuel capacity, allowing for a much greater delta-v for your long hauls
through space.

Since it is a passive upgrade, no configuration or activation is
necessary to take advantage of its functionality.

Copyright © 2408 Songket Shipyards, All Rights Reserved.
