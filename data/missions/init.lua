local mail = require("mail")
local mission = require("mission")
local utils = require("utils")

local coro = coroutine.create(function(ship)
      while(not ship.events.engine_restart) do
         ship.engine_on = false
         coroutine.yield()
      end

      mail.deliver_msg(ship, "dex19-2.msg")
      mission.accept(ship, "buy_battery")

      mission.wait_for(ship, function()
            if(utils.time(ship) - ship.events.engine_restart > 640) then
               mail.deliver_msg(ship, "dex19-2b.msg")
            end
            return ship:in_range(utils.find_by(ship.bodies, "name",
                                               "Merdeka Station"))
      end)
      mail.deliver_msg(ship, "dex19-3.msg")
end)

return {
   init = function(ship) table.insert(ship.updaters, coro) end,
   on_success = function(ship)
      mail.deliver_msg(ship, "dex19-4.msg")
      mail.deliver_msg(ship, "cmec-recruit.msg")
   end,

   -- TODO: I want to redo this, because it's kind of lame that the first thing
   -- you do upon starting the game is go find a company to work for.
   -- Instead of needing to find a job to make money for ship repairs, you sign
   -- up for the training program at CMEC specifically so you'll get access to
   -- their repair drones, and you use those to fix the connections to the
   -- battery on your own ship. So you still have to go thru their training
   -- course, but instead of getting paid at the end (which doesn't make any
   -- sense anyway) you take the drone and land it on your own ship, so we need
   -- a map of the ship.

   credits=512,
   invisible=true,
   objectives={"trainee01"},
}
