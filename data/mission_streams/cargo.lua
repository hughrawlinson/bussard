local lume = require("lume")
local mail = require("mail")
local mission = require("mission")
local systems = require("data.systems")

local generate_cargo_run = function (ship, from, to, credits, old_id)
   local id = old_id or lume.uuid()
   local new_mission = {
      id = id,
      name = "Delivery from " .. from .. " to " .. to,
      description = "Delivery from " .. from .. " to " .. to,
      credits = credits,
      destinations = {from, to, },
      success_message = "Thanks for the delivery. " ..
         "Check back later — we often have these boring delivery orders.",
      success_events = {id,},
      regenerate =
         "return require('data.mission_streams.cargo')." ..
         "generate_cargo_run(ship, '" ..
         from .. "', '" .. to .. "', " .. credits .. ", '" .. id ..
         "').mission",
      generator = "cargo",
   }
   local from_system = nil
   local to_system = nil
   for system_name,system in pairs(systems) do
      for _, body in ipairs(system.bodies) do
         if (body.name == from) then from_system = system_name end
         if (body.name == to) then to_system = system_name end
      end
   end
   if(not (from_system and to_system)) then return nil, "Mission impossible" end
   local full_from = from .. " (" .. from_system .. ")"
   local full_to = to .. " (" .. to_system .. ")"
   local message =
      "From: LongSpaceHaul.com\n" ..
      "To: jobs@news.local\n" ..
      "Subject: " .. new_mission.description .. "\n" ..
      "Content-Type: text/plain; charset=UTF-8" ..
      "Message-Id: " .. id  .. "\n\n" ..
      "Pick up: " .. full_from .. "\n" ..
      "Drop of: " .. full_to .. "\n" ..
      "Delivery payment: " .. credits .. "\n" ..
      ""
   if(not old_id) then
     mail.deliver_msg(ship, id, message)
     mission.add_mission(new_mission)
   end
   return {
      mission = new_mission,
      message = message,
   }
end

local generate_random_tana_cargo_run = function(ship)
   local worlds = {}
   for _, system in pairs(systems) do
      if(system.gov == "Tana") then
         for _, body in ipairs(system.bodies) do
            if (body.os and not body.portal) then
               table.insert(worlds, body.name)
            end
         end
      end
   end
   local from
   local to
   from = math.random(1,#worlds)
   to = from
   while(to==from) do
      to = math.random(1,#worlds)
   end
   from = worlds[from]
   to = worlds[to]
   local credits = math.random(3,50)*10
   return generate_cargo_run(ship, from, to, credits)
end

return {
   generate_cargo_run = generate_cargo_run,
   generate_random_tana_cargo_run = generate_random_tana_cargo_run,
}
