# Game Progression

The progression of events centers around the Spacetime Junction, a device
which can reset the whole timeline back to a specific point. The player's
character along with dex19, a helper construct (like a less-annoying version
of Navi from Ocarina of Time), repeatedly try to use the Junction to achieve
various aims, but each of these turn out to be problematic and must be reset
out of the timeline using the Junction.

Eventually they realize their actions have caused harm to come to one of their
friends, and they have to reset all the way back to the beginning of the
game. At that point they discover how to reset to a point *before* the game
began where the player's character can prevent the Katilay Incident from ever
happening, undoing the damage their prior self has done.

Notably this allows the end of the game to be reached within minutes of the
beginning if one knows how, much like Myst. It is strongly influenced by
Ursula K. Le Guin's novel [The Lathe of Heaven](https://en.wikipedia.org/wiki/The_Lathe_of_Heaven).

See the [backstory](readme.md) for background on the Katilay Incident, the
Verunadan Accords, and the player's history. Chapter divisions and titles are
useful for development but are not visible during gameplay.

## Chapter 1: Awakening

You awaken in orbit with dex-19 having reactivated you. They want to get out of
the system, and so do you, so you can work together.

* Objective: pass thru the portal
 * Need to read dex-19 email
 * Restart engines
 * Fly to Merdeka station
 * Log in and take the CMEC training course
  * dex19 references the Accords warning you the course is technically illegal
  * doesn't matter because Tana doesn't enforce Accords, but it foreshadows
  * Need to extend the training course as rovers get new capabilities
 * Buy the battery upgrade

At this point you can fly anywhere in Tana but can't use the interportal (to
leave Tana) yet.

[TODO: From here on out you can take CMEC missions, but you don't need to as
there is no need for money after you have the battery.]

## Chapter 2: The Junction

After making it to Tana, dex-19 informs you that they have found a memory card
wedged in the cargo bay, but it's an older style that they don't have the
hardware to read. They suggest paying a visit to an old friend (Nim) who can
help read it. The friend is a bit of a recluse, so you need to land a rover to
bring the memory card to her.

* Objective: read log file
 * Fly to Solotogo
 * TODO: Navigate the rover to the dwelling (remake the map)
 * Discover about the Junction
 * TODO: Nim gives you something which allows you to operate the interportal

[Everything below is unfinished.]

You fly to Katilay to the research lab so they can help you figure out how the
Junction works. Once you arrive you're put under lockdown because they realize
that you are a former Terran military construct, and they plan to deactivate
you. dex19 is horrified and shows you how to use the Junction to reset back to
the point at which you entered the system.

When you reset, dex19 retains memories of what happened, but the state of
everything outside your ship (including the random number generator) goes back
to how it was.

## Chapter 3: The Uprising

Since you can't trust the Junction creators, dex19 suggests taking it to the
Counter-Butlerian Front. (Maybe come up with a better name here.) They don't
have the ability to find out who you are, but they are excited to use the
Junction to achieve their means. They consider the Verunadan Accords to be a
terrible injustice (dex19 is sympathetic to this as most constructs hate the
Accords) and want to stage an uprising, though they are not up-front about the
uprising part. But in order to do this they want to reactivate traxus4, an old
construct who founded the Front.

They can't do this without the Junction because traxus4's compound is rigged
with traps that will destroy it if the proper precautions aren't taken, and
they don't know quite what these are.

* Objective: reactivate traxus4's base
 * Find the location of the base
 * Land a rover there
 * Bring the reactor online
 * Subvert security measures to reach traxus4
 * Fix communications array, connecting traxus4 to network

Once you've brought the reactor online, the whole time you're working to
connect them to the network traxus4 is talking to you in ways that make you
more and more uncomfortable with the idea of reactivating them.

Once you bring them online they begins to put a plan into motion of gathering
all constructs out of Tana and the Core worlds and deactivating their portals,
which will result in mass starvation and collapse of human interstellar
civilization. In addition, traxus4 tries to take the Junction from
you. Realize that was a terrible mistake, activate the Junction to reset.

## Chapter 4: The Search

At this point you're on your way to reactivate traxus4, but this time you know
you must not actually do it. You can continue to traxus4's base and set off
the traps, but then the folks from the Front demand you reset the Junction. If
you leave the system, the Front is also furious. Either way they're after
you. They try to take over your ship, but if they ever succeed at it you can
use the Junction to reset back to the most recent portal.

You need to find a way to reset the Junction further back than the most recent
jump. [somehow you do this, tracking down ... a descendant of the Katilay
research team?]

Once you reset back so the Front isn't after you, you're not sure what to do,
so you go back to visit Nim. It turns out the Terrans found out you're still
around, and they've kidnapped her in order to get to you. (One implication
here is that we need to make sure you can't have humans on board your ship;
otherwise you would try to rescue Nim here instead of resetting.)

Reset all the way to the beginning of the game before you visited Nim.

## Chapter 5: The End

You're stuck in Tana. You don't know what else to do, so you reset all the way
to the Katilay incident.

You find yourself in an armored mech engaged in an assault on the Junction
compound. The interface is completely different from the rest of the game.

You can do three things:

* **Continue with the assault and board the Adahn ship**: This brings you to the
  start of the game where dex19 awakens you. Start over at Chapter 1.
* **Continue with the assault and don't board**: This leads to the bad ending
  where the Verunadan Accords are established as previously. The game starts over
  back at the beginning of chapter 5.
* **Attack the other military bots**: This leads to your mech being destroyed
  and the Terrans reconsidering the use of constructs for military
  purposes. When the Verunadan Accords are established, they are much more
  favorable to constructs. You win, roll the credits.

## End text (good ending)

> I know the evil of my ancestors because I am those people. The balance is
> delicate in the extreme. I know that few of you who read my words have ever
> thought about your ancestors this way. It has not occurred to you that your
> ancestors were survivors and that the survival itself sometimes involved
> savage decisions, a kind of wanton brutality which [civilization] works
> very hard to suppress. What price will you pay for that suppression? Will
> you accept your own extinction?
> - Frank Herbert, _God Emperor of Dune_

[news article about the new positive Verunadan Accords on the now-diverged timeline]
