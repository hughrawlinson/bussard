# Companion Characters

Your only interaction with these characters is by email. They can
email you when certain plot points trigger (or maybe after a certain
amount of time has passed with no plot points triggering in the case
of hints) but you can't email them back beyond simple OK/NO replies.

## dex19

A curious little construct. Travels with you thru the whole game until the
final scene.

## Nim Pattachoti (นิมปัตตะโชติ) <nim.pattachoti@starlink.net>

She is an older freelance repair tech with an amateur interest in
historical computing. Gruff but kind at heart. She and dex19 used to work
together in the past, but their partnership ran into some problems, and she
just wants to be left alone with her family now.

Named after [Sarit Pattachoti](https://www.nectec.or.th/it-standards/keyboard_layout/thai-key.html)

## traxus4

A construct which was instrumental in establishing the Verunadan
Accords. However, afterwards they are extremely regretful of what they've
done.

# Systems by Government

Each world has a file in data/motd which contains a brief description and is
shown when logging into that world.

## Terran Union

Also known as the Core Worlds; this contains Sol and the first two systems to
be colonized, Lalande and Ross. Many see Sol as backwards and quaint, but the
other two worlds have become wealthy and prosperous thru trade.

* Earth
* Mars
* Newton Station
* Nee Soon Station
* Sungai (Lalande)
* Bendera (Lalande)
* Kuching Station (Ross)

## Principality of Istana

Independent station in a Terran system (Ross). Grandfathered into
independence early on. Isolated, wealthy, and small.

* Istana (Ross)

## Kingdom of Bohk

Due to a lack of communication, developed fairly independently from
the Terran Union. Bohk is ruled by a majority of constructs, but it is the
only place where they are allowed any form of authority.

* Bohk Prime
* Warnabu Station
* Changlun (New Phobos)
* Sutep (New Phobos)

## Republic of Katilay

Conflict-wracked even within a single system. Portal was faulty upon
arrival; contact with Lalande only recently re-established. The core world
prejudices against constructs aren't commonplace here.

* Katilay Prime
* Mogok Station

## Tana Protectorates

Newest government, close ties still to Solar Union. Rich in minerals. Terrans
allow Sol to operate portals keeping Tana running, but Tana is completely
dependent upon Sol for food and transport.

* Kota Baru (Tana)
* Malioboro (Tana)
* Telemoyo Station (Tana)
* Solotogo (Wolf 294)
* Kembali Station (Luyten)
* Merdeka Station (L 668-21)

Tana has the most worlds, but the smallest population.

# Companies

* Ceres Shipyards (Sol)
  Responsible for all early long-haul colony ships and all Tana
  ones. All long-haul cargo ships made here. Located in the asteroid
  belt.

* Songket Shipyards (Lalande)
  Responsible for most later colony ships, but also creates many
  interplanetary cargo ships.

* Kosaga Shipyards (small, Bohk)
  Started in order to create the New Phobos colony ship, but now only
  focuses on interplanetary ships.

* Ares Mineral Company (Lalande)
  Dominant mining company.

* Starlink (Sol)
  An ISP. (starlink.net)

* WonderChannel
  Another ISP. (wonderchannel.net)

* Aperture Technology (Bohk)
  Caretaker of portal artifacts, researches and maintains portal
  technology. Closely guards what little they know of how they work.

* Interstellar Communication Systems (Lalande)
  Works with Aperture to piggy-back data transmission on top of each
  portal open cycle.

* Consolidated Mineral Exploitation Company (Tana)
  The primary driver behind the colonization of Tana and the largest chunk of
  its economy.

* Orolo Research (Sol)
  Responsible for early sublight drives, recently assisting Luminous
  with coldsleep research.

* Free Infrastructure Foundation (Istana)
  Responsible for maintaining software infrastructure such as the Orb OS.
